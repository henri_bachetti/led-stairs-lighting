# LED STAIRS LIGHTING

The purpose of this page is to explain step by step the realization of a LED stairs lighting system based on ARDUINO MEGA.

It includes a dimmer for progressive lighting.

The board uses the following components :

 * an ARDUINO NANO
 * two HC-SR501 motion sensor
 * 15 IRLD024 MOSFETS
 * some passive components
 * the board is powered by a switching power supply.

### ELECTRONICS

The schema is made using KICAD.

### ARDUINO

The code is build using ARDUINO IDE 1.8.5.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2019/03/un-eclairage-descalier-leds.html