
#include <avr/sleep.h>
#include <Bounce2.h>

#define DEBUG

#define UPSTAIRS      1
#define DOWNSTAIRS    2
#define INCREASE      1
#define DECREASE      2
#define DOWN_PIR_PIN  20
#define UP_PIR_PIN    21

class DimmingLed
{
    static int staticId;
  private:
    int _ledId;
    int _ledPin;
    int _endValue;
    int _curValue;
    int _step;
    int _running;

  public:
    DimmingLed(int pin);
    int pin(void);
    int value(void);
    int get_endValue(void);
    void set_endValue(int value);
    void tick(void);
    void start(void);
    void stop(void);
    int started(void);
};

int DimmingLed::staticId = 0;

DimmingLed::DimmingLed(int pin)
{
  _ledId = staticId++;
  _ledPin = pin;
  _endValue = 0;
  _curValue = 0;
  _step = 0;
  _running = false;

}

int DimmingLed::pin(void)
{
  return _ledPin;
}

int DimmingLed::value(void)
{
  return _curValue;
}

int DimmingLed::get_endValue(void)
{
  return _endValue;
}

void DimmingLed::set_endValue(int value)
{
  _endValue = value;
  _step = (_endValue - _curValue) < 0 ? -1 : 1;
}

void DimmingLed::tick(void)
{
  int changed = false;

  if (_running) {
    if ((_endValue > 0 && _curValue < _endValue) || (_endValue == 0 && _curValue > 0)) {
      _curValue += _step;
      changed = true;
    }
    analogWrite(_ledPin, (int)(_curValue / 100.0 * 255));
    if (changed && _curValue == _endValue) {
      _running = false;
    }
#ifdef DEBUG
    Serial.print(micros());
    Serial.print(" DimmingLed::tick[");
    Serial.print(_ledId);
    Serial.print("] (pin");
    Serial.print(_ledPin);
    Serial.print("):");
    Serial.print(_curValue);
    Serial.print("/");
    Serial.print(_endValue);
    Serial.print(" (");
    Serial.print(_running);
    Serial.println(")");
#endif
  }
}

void DimmingLed::start(void)
{
#ifdef DEBUG
  Serial.print(micros());
  Serial.print(" DimmingLed::start[");
  Serial.print(_ledId);
  Serial.println("] ");
#endif
  _running  = true;
}

void DimmingLed::stop(void)
{
#ifdef DEBUG
  Serial.print(micros());
  Serial.print(" DimmingLed::stop[");
  Serial.print(_ledId);
  Serial.println("] ");
#endif
  _running  = false;
}

int DimmingLed::started(void)
{
  return _running;
}

class Dimmer
{
  private:
    DimmingLed *_dimmingLeds;
    int _nLeds;
    int _dimmingDelay;
    int _onLedTrigger;
    int _offLedTrigger;
    unsigned long _lastStepTime;
    int _direction;
    int _action;
    int _maxLuminosity;
    int _currentLed;
    void _nextLed(void);

  public:
    Dimmer(DimmingLed *leds, int n, int delay, int turnOnTrigger, int turnOffTrigger, int luminosity);
    void turnOn(int dir);
    void turnOff(int dir);
    void setDelay(int delay);
    void step();
    bool isIdle(void);
};

// leds: DimmingLed array
// n: number of LEDs
// delay: FADING delay (µs)
// next: LED start delay (0-100%)
// luminosity: (0-100%)

Dimmer::Dimmer(DimmingLed *leds, int n, int delay, int turnOnTrigger, int turnOffTrigger, int luminosity)
{
  _dimmingLeds = leds;
  _nLeds = n;
  _dimmingDelay = delay;
  _onLedTrigger = min(turnOnTrigger, 100);
  _offLedTrigger = min(turnOffTrigger, 100);
  _maxLuminosity = min(luminosity, 100);
  _lastStepTime = micros();
  _direction = 0;
  _action = 0;
  _currentLed = 0;
}

void Dimmer::turnOn(int dir)
{
  _action = INCREASE;
  _direction = dir;
  _currentLed = dir == UPSTAIRS ? 0 : _nLeds - 1;
  for (int i = 0 ; i < _nLeds ; i++) {
    _dimmingLeds[i].set_endValue(_maxLuminosity);
    _dimmingLeds[i].stop();
  }
#ifdef DEBUG
  Serial.print("turnOn ");
  Serial.print(_currentLed);
  Serial.print(" _direction ");
  Serial.println(_direction);
#endif
  _dimmingLeds[_currentLed].start();
}

void Dimmer::turnOff(int dir)
{
  _action = DECREASE;
  _direction = dir;
  _currentLed = dir == UPSTAIRS ? 0 : _nLeds - 1;
  for (int i = 0 ; i < _nLeds ; i++) {
    _dimmingLeds[i].set_endValue(0);
    _dimmingLeds[i].stop();
  }
#ifdef DEBUG
  Serial.print("turnOff ");
  Serial.print(_currentLed);
  Serial.print(" _direction ");
  Serial.println(_direction);
#endif
  _dimmingLeds[_currentLed].start();
}

void Dimmer::step()
{
  static unsigned long _lastStepTime;
  DimmingLed *pLed = &_dimmingLeds[_currentLed];
  unsigned long currentTime  = micros();
  if (currentTime > _lastStepTime + _dimmingDelay) {
    _lastStepTime = currentTime;
    pLed->tick();
    if (pLed->started()) {
      if (_action == INCREASE) {
        if (pLed->value() >= (pLed->get_endValue() * _onLedTrigger / 100)) {
          if (_direction == UPSTAIRS) {
            if (_currentLed < _nLeds - 1) {
              pLed++;
              if (!pLed->started()) {
                pLed->start();
              }
            }
          }
          else {
            if (_currentLed > 0) {
              pLed--;
              if (!pLed->started()) {
                pLed->start();
              }
            }
          }
        }
      }
      else if (_action == DECREASE) {
        if (pLed->value() <= (_maxLuminosity * (100 - _offLedTrigger) / 100)) {
          if (_direction == UPSTAIRS) {
            if (_currentLed < _nLeds - 1) {
              pLed++;
              if (!pLed->started()) {
                pLed->start();
              }
            }
          }
          else {
            if (_currentLed > 0) {
              pLed--;
              if (!pLed->started()) {
                pLed->start();
              }
            }
          }
        }
      }
    }
    _nextLed();
  }
}

void Dimmer::_nextLed(void)
{
  if (_direction == UPSTAIRS) {
    _currentLed = _currentLed == _nLeds - 1 ? 0 : _currentLed + 1;
  }
  else {
    _currentLed = _currentLed == 0 ? _nLeds - 1 : _currentLed - 1;
  }
}

void Dimmer::setDelay(int delay)
{
  _dimmingDelay = delay;
}

bool Dimmer::isIdle(void)
{
  for (int i = 0 ; i < _nLeds ; i++) {
    if (_dimmingLeds[i].started()) {
      return false;
    }
  }
  return true;
}

DimmingLed leds[] = {DimmingLed(2), DimmingLed(3), DimmingLed(4),
                     DimmingLed(5), DimmingLed(6), DimmingLed(7), DimmingLed(8),
                     DimmingLed(9), DimmingLed(10), DimmingLed(11), DimmingLed(12),
                     DimmingLed(13), DimmingLed(44),
                     DimmingLed(45), DimmingLed(46)
                    };
int _nLeds = sizeof(leds) / sizeof(leds[0]);

#define DIMMER_DELAY  1950
#define OFF_DELAY     10000

// leds: DimmingLed array
// n: 13 LEDs
// delay: 1ms
// onTrigger: 25%
// offTrigger: 3%
// luminosity: 50%
Dimmer dimmer(leds, _nLeds, DIMMER_DELAY, 25, 3, 50);
//Dimmer dimmer(leds, _nLeds, DIMMER_DELAY, 50, 50);
//Dimmer dimmer(leds, _nLeds, DIMMER_DELAY, 1, 50);

Bounce downDebouncer = Bounce();
Bounce upDebouncer = Bounce();

void setup() {
  Serial.begin(115200);
  Serial.println("FadeIn FadeOut");
  for (int i = 0 ; i < _nLeds ; i++) {
    pinMode(leds[i].pin(), ANALOG_OUTPUT);
  }
  pinMode(DOWN_PIR_PIN, INPUT);
  pinMode(UP_PIR_PIN, INPUT);
  downDebouncer.attach(DOWN_PIR_PIN);
  downDebouncer.interval(5);
  upDebouncer.attach(UP_PIR_PIN);
  upDebouncer.interval(5);
}

void loop()
{
  static unsigned long start, wakeup;
  static int direction;

  upDebouncer.update();
  downDebouncer.update();
  if (downDebouncer.rose()) {
    Serial.println("DOWN");
    direction = DOWNSTAIRS;
    dimmer.setDelay(DIMMER_DELAY);
    dimmer.turnOn(DOWNSTAIRS);
    start = millis();
  }
  if (upDebouncer.rose()) {
    Serial.println("UP");
    direction = UPSTAIRS;
    dimmer.setDelay(DIMMER_DELAY);
    dimmer.turnOn(UPSTAIRS);
    start = millis();
  }
  if (direction) {
    unsigned long now  = millis();
    unsigned long elapsed = now - start;
    if (elapsed >= OFF_DELAY) {
      dimmer.setDelay(DIMMER_DELAY * 2);
      dimmer.turnOff(direction);
      direction = 0;
    }
  }
  dimmer.step();
  if (!direction) {
    unsigned long now  = millis();
    unsigned long elapsed = now - wakeup;
    // wait some time > debouncer interval
    if (elapsed >= 50) {
      if (dimmer.isIdle()) {
        Serial.println("Goto sleep");
        delay(10);
        sleepNow();
        wakeup = millis();
        Serial.println("Wake up");
        delay(10);
      }
    }
  }
}

void wakeUpNow()
{
}

void sleepNow()
{
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_enable();
  attachInterrupt(digitalPinToInterrupt(20), wakeUpNow, RISING);
  attachInterrupt(digitalPinToInterrupt(21), wakeUpNow, RISING);
  sleep_mode();
  sleep_disable();
  detachInterrupt(digitalPinToInterrupt(20));
  detachInterrupt(digitalPinToInterrupt(21));
}

